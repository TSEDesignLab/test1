/* File:   main.c
 * Author: antz
 * Created on 13 September 2017
 */
// CONFIG1
#pragma config FOSC = INTOSC    //  (INTOSC oscillator; I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable (Brown-out Reset disabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config PLLEN = OFF      // PLL Enable (4x PLL disabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LPBOREN = OFF    // Low Power Brown-out Reset enable bit (LPBOR is disabled)
#pragma config LVP = OFF         // Low-Voltage Programming Enable (Low-voltage programming enabled)
// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.
#include <xc.h>
#include "main.h"

void initializers(void);
void blink_LED(void);
void blink_Relay(void);
void Delay_Sec(unsigned char sec);

void main(void) {
    initializers();
    
    //RELAY_ON;
    
    while(1)
    {
        blink_LED();
        blink_Relay();
    }
    return;
}

/**********************************
    Processor Port/Pin Initialize
**********************************/
void initializers(void)
{
    /*** set corresponding tris bits to inputs and outputs ***/
	TRISA = 0x0F;               // (MCLR/ICSP = Inputs) 0000 1111
	// Clear all Ports
	PORTA = 0X00;
    /* OSC SET UP */
    OSCCON = 0x6A;              // IRCF Bits for 4MHz (pg 42)
}

/* Delay for X seconds */
void Delay_Sec(unsigned char sec)
{
    unsigned char x, y;
    
    for(y = 0; y < sec; y++){
        for(x = 0; x < 100; x++){
            __delay_ms(10);
        }
    }
}

/*  */
void blink_Relay(void)
{
    RELAY_ON;
    Delay_Sec(1);       //__delay_ms(250);
    RELAY_OFF;
    Delay_Sec(1);       //__delay_ms(250);
}

/*  */
void blink_LED(void)
{
    STATUS_ON;
    __delay_ms(250);
    STATUS_OFF;
    __delay_ms(250);
}